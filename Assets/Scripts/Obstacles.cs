﻿using UnityEngine;
using System.Collections;

public class Obstacles : MonoBehaviour {

    public GameObject obstacle = null;
    float cycle = 1f;
    float timeElapsed = 0f;

	void Update () {
        timeElapsed += Time.deltaTime;
        if(timeElapsed > cycle)
        {
            Instantiate(obstacle);
            timeElapsed -= cycle;
        }
    }
}
