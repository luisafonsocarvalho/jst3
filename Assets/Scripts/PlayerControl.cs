﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

    public ControllerScript control;

    Rigidbody rb;
    Animation anim;
    private bool isGrounded = true, dead = false, respawn = false, firstGround = true;
    public float speed, jumpSpeed;
    public AudioClip crashSound, jumpSound, fallingSound;
    private AudioSource src;
    private float offset = 400;
    private static float JUMP_FACTOR = 0.7f;

    void Start () {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animation>();
        src = GetComponent<AudioSource>();
    }

    void FixedUpdate () {
        if (dead)
            return;
        if (isGrounded) {
            anim.Play("run");
        }
        float velX = 0, velY = rb.velocity.y, velZ = speed;

        if (Input.GetKeyDown("right")) {
            velX = offset;
        }
        if (Input.GetKeyDown("left")) {
            velX = -1 * offset;
        }

        rb.velocity = new Vector3(velX, velY, velZ);;
        if (!isGrounded) {
            rb.velocity = new Vector3(0, rb.velocity.y, speed * JUMP_FACTOR);
        }
        if (Input.GetKeyDown("space") && isGrounded) {
            anim.Stop("run");
            anim.Play("jump");
            src.PlayOneShot(jumpSound, 1);
            isGrounded = false;
            rb.AddForce(0, jumpSpeed, 0, ForceMode.Impulse);
        }
    }

    void OnCollisionEnter(Collision c) {
        if (c.collider.name == "Ground") {
            anim.Play("run");
            anim.Stop("jump");
            isGrounded = true;
            if (!firstGround)
                respawn = true;
            if (firstGround)
                firstGround = false;
        }
    }


    void OnCollisionExit(Collision c) {
        if (respawn) {
            respawn = false;
            return;
        }
        if (c.collider.name == "Ground" && isGrounded) {
            src.Stop();
            src.PlayOneShot(fallingSound);
            dead = true;
            anim.Stop("run");
            anim.Stop("jump");
            anim.Play("death");
            rb.velocity = new Vector3(0, -9.8f * Time.deltaTime, 0);
            Invoke("gameOver", 1f);
        }
    }

    void OnTriggerEnter(Collider c) {
        if (c.gameObject.name == "Trigger") {
            control.respawnGround();
        }
        if (c.gameObject.tag == "obst") {
            dead = true;
            anim.Stop("run");
            anim.Stop("jump");
            src.Stop();
            src.PlayOneShot(crashSound, 1);
            anim.Play("death");
            rb.velocity = new Vector3(0, 0, 0);
            Invoke("gameOver", 1f);
        }
    }

    void gameOver() {
        control.gameIsOver();
    }
}
