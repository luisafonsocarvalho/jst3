﻿using UnityEngine;
using System.Collections;

public class ObstacleController : MonoBehaviour {

    private int direction = 1;

    public float slideSpeed = 3f;

    private static int EDGE = 7;

    void Start () {
        if (transform.position.x == EDGE)
            direction = -1;
        float xPos = Random.value * EDGE * 2 - EDGE;
        transform.position = new Vector3(xPos, transform.position.y, transform.position.z);
    }
}
