﻿using UnityEngine;
using System.Collections;

public class ControllerScript : MonoBehaviour {
    private float score = 0;
    private bool gameOver = false;
    private static int PLANE_UNIT = 10;
    public GameObject groundObject;
    private GameObject prevObject = null;

    void Start () {
        gameOver = false;
        Time.timeScale = 1;
    }

    void Update () {
        if (gameOver)
            return;
        this.score += Time.deltaTime;
    }
    void OnGUI() {
        if (!gameOver) {
            GUI.Label(new Rect(Screen.width - (Screen.width / 6), 10, Screen.width / 6, Screen.height / 6), "SCORE: " + ((int)score).ToString());
        }
        else
        {
            Time.timeScale = 0;
            //display the final score
            GUI.Box(new Rect(Screen.width / 4, Screen.height / 4, Screen.width / 2, Screen.height / 2), "GAME OVER\nYOUR SCORE: " + (int)score);
            //restart the game on click
            if (GUI.Button(new Rect(Screen.width / 4 + 10, Screen.height / 4 + Screen.height / 10 + 10, Screen.width / 2 - 20, Screen.height / 10), "RESTART"))
                Application.LoadLevel(Application.loadedLevel);
            if (GUI.Button(new Rect(Screen.width / 4 + 10, Screen.height / 4 + 3 * Screen.height / 10 + 10, Screen.width / 2 - 20, Screen.height / 10), "EXIT GAME"))
                Application.Quit();
        }
    }

    public void gameIsOver() {
        gameOver = true;
    }

    public void respawnGround() {
        GameObject nextGround = (GameObject)Instantiate(groundObject);
        float translateZ = groundObject.transform.Find("Ground").localScale.z * PLANE_UNIT;
        nextGround.transform.Translate(0, 0, translateZ);
        prevObject = groundObject;
        groundObject = nextGround;
        Invoke("removeGround", 2f);
    }

    private void removeGround() {
        if (!gameOver)
            Destroy(prevObject);
    }
}
